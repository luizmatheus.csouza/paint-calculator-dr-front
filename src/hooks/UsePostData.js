//hooks
import { useState } from "react"

//service
import { postData } from "../service/ApiService"

export const UsePostData = (endpoint) => {
  
  const [loading, setLoading] = useState(false)
  const [response, setResponse] = useState("")

  const post = async (data) => {
    setLoading(true)
    try {
      const result = await postData(endpoint, data)
      setResponse(result)
    }finally{
      setLoading(false)
    }
  }
  return {post, loading, response}
}