//validator
import { validatorsMeasures } from "../../validators/validatorsMeasures";

//components
import Error from '../Error/Error';

//hooks
import { useState } from "react";

function CardMeasures({ id, initialData, updateValues }) {

  const [widthWall, setWidthWall] = useState(initialData.widthWall);
  const [heightWall, setHeightWall] = useState(initialData.heightWall);
  const [quantityDoor, setQuantityDoor] = useState(initialData.quantityDoor);
  const [quantityWindow, setQuantityWindow] = useState(initialData.quantityWindow);
  const [errorMessage, setErrorMessage] = useState("");

  const handleBlur = () => {
    const error = validatorsMeasures(widthWall, heightWall);
    if (error) {
      setErrorMessage(error);
    } else {
      setErrorMessage("");
      updateValues(id, { widthWall, heightWall, quantityDoor, quantityWindow });
    }
  };

  return (
    <>
      <label htmlFor={`width-${id}`}>Largura</label>
      <input type="number" required id={`width-${id}`} onChange={(e) => setWidthWall(e.target.value)} onBlur={handleBlur} value={widthWall} />

      <label htmlFor={`height-${id}`}>Altura</label>
      <input type="number" required id={`height-${id}`} onChange={(e) => setHeightWall(e.target.value)} onBlur={handleBlur} value={heightWall}/>

      <label htmlFor={`door-${id}`}>Quantidade de portas</label>
      <input type="number" required id={`door-${id}`} min="0" step="1" onChange={(e) => setQuantityDoor(e.target.value)} onBlur={handleBlur} value={quantityDoor} />

      <label htmlFor={`window-${id}`}>Quantidade de janelas</label>
      <input type="number" required id={`window-${id}`} min="0" step="1" onChange={(e) => setQuantityWindow(e.target.value)} onBlur={handleBlur} value={quantityWindow} />

      {errorMessage && <Error message={errorMessage} />}
    </>
  );
}

export default CardMeasures;
