//Context
import { useContext } from 'react';

//Navigate
import { Navigate } from 'react-router-dom';

//Context
import { DataContext } from '../../context/DataContext';

const ProtectedRoute = ({ element }) => {
  const { data } = useContext(DataContext);

  const isValid = data.widthWall !== null && data.heightWall !== null && data.quantityDoor !== null && data.quantityWindow !== null;

  return isValid ? element : <Navigate to="/" />;
};

export default ProtectedRoute;
