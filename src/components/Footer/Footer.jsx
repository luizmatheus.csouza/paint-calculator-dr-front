import styles from "./Footer.module.css"

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <h3>Developed by Luiz Matheus</h3>
      <p>PaintCalculator &copy; 2024</p>
    </footer>
  )
}

export default Footer
