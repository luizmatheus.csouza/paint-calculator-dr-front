import styles from './CardCan.module.css'

function CardCan({id, canQuantity}) {
  const renderizerCanQuantity = () =>{
    if(id === 'can0' && canQuantity !== 0){
      return (
        <div key={id} className={styles.containerCard}>
          {`${canQuantity} Latas de 18 litros`}
        </div>
      )
    }
    if(id === 'can1' && canQuantity !== 0){
      return (
        <div key={id} className={styles.containerCard}>
          {`${canQuantity} Latas de 3,6 litros`}
        </div>
      )
    }
    if(id === 'can2' && canQuantity !== 0){
      return (
        <div key={id} className={styles.containerCard}>
          {`${canQuantity} Latas de 2,5 litros`}
        </div>
      )
    }
    if(id === 'can3' && canQuantity !== 0){
      return (
        <div key={id} className={styles.containerCard}>
          {`${canQuantity} Latas de 0,5 litros`}
        </div>
      )
    }
  }

  return (
    <>
      {renderizerCanQuantity()}
    </>
  )
}

export default CardCan
