import styles from './Button.module.css'

function Button({textButton, clicked, checkButton}) {
  return (
    <>
      <button className={styles.btn} onClick={clicked} disabled={checkButton}>{textButton}</button>
    </>
  )
}

export default Button
