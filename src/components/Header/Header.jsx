//CSS
import styles from "./Header.module.css"

// navigate
import { NavLink } from "react-router-dom"

const Header = () => {
  return(
    <header className={styles.header}>
      <NavLink to="/"><h1><span>Paint</span>Calculator</h1></NavLink>
    </header>
  )
}

export default Header