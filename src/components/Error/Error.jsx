//CSS
import styles from "./Error.module.css"

function Error({message}) {
  return (
    <>
      <h2>{message}</h2>
    </>
  )
}

export default Error
