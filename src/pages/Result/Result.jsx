//css
import styles from './Result.module.css'

//navigate
import { useNavigate } from 'react-router-dom';
import { useLocation } from 'react-router-dom';

//components
import Button from '../../components/Button/Button';
import CardCan from '../../components/CardCan/CardCan';

function Result() {

  const navigate = useNavigate();
  
  const location = useLocation();
  const { response } = location.state;

  let arrayResponse = Object.keys(response).map((res) => response[res])
  const arrayCans = arrayResponse.slice(0,4)


  const createComponent = () => {
    const elements = [];
    for (let i = 1; i <= 4; i++) {
      
    }
    arrayCans.forEach((e,index)=>{
      
      const canKey = `can${index}`;
      
      elements.push(
        <CardCan id={canKey} canQuantity={e} />
      );
    })
    return elements;
  };

  const handleButtonClick = () => {
    navigate('/');
  };
  

  return (
    <div className={styles.container}>
      <h1>Resultado</h1>
      <div className={styles.result}>
        <p>A área total informada foi de: <span>{response.totalAreaWall}m²</span>, para essa medida você vai precisar de:</p>
        <h3>{response.liters} litros</h3>
        <h2>As latas de tinta necessárias serão:</h2>
        {createComponent()}
        {response.restLiter === 0 ? "" : <p>Olha só, ainda vai te sobrar <span>{response.restLiter} Litros</span> de tinta</p>}
      </div>
      <Button textButton={"Calcular com outros valores"} clicked={handleButtonClick}/>
    </div>
  );
}

export default Result

