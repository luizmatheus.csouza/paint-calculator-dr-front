// CSS
import styles from './CalculatePaint.module.css';

// Router
import { useNavigate } from 'react-router-dom';

// Context
import { DataContext } from '../../context/DataContext';

// Component
import CardMeasures from '../../components/CardMeasures/CardMeasures';
import Error from '../../components/Error/Error';
import Button from '../../components/Button/Button';

// Hooks
import { useContext, useEffect, useState } from 'react';
import { UsePostData } from '../../hooks/UsePostData';

function CalculatePaint() {
  const { data } = useContext(DataContext);
  const navigate = useNavigate();
  const { post, loading, response } = UsePostData("calculate");

  const [inputs, setInputs] = useState({
    wall1: { widthWall: data.widthWall, heightWall: data.heightWall, quantityDoor: data.quantityDoor, quantityWindow: data.quantityWindow },
    wall2: { widthWall: data.widthWall, heightWall: data.heightWall, quantityDoor: data.quantityDoor, quantityWindow: data.quantityWindow },
    wall3: { widthWall: data.widthWall, heightWall: data.heightWall, quantityDoor: data.quantityDoor, quantityWindow: data.quantityWindow },
    wall4: { widthWall: data.widthWall, heightWall: data.heightWall, quantityDoor: data.quantityDoor, quantityWindow: data.quantityWindow },
  });

  const [shouldNavigate, setShouldNavigate] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    if (!data) {
      navigate('/');
    }
  }, [data, navigate]);

  useEffect(() => {
    if (shouldNavigate && response) {
      if(response.statusCode === 400){
        setErrorMessage(response.message)
        setShouldNavigate(false)
      }else{
        navigate('/resultado', { state: { response } });
      }
    }
  }, [shouldNavigate, response, navigate]);

  const updateValues = (id, values) => {
    setInputs(prevState => ({
      ...prevState,
      [id]: values,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    let body = [];
    body.push(inputs.wall1, inputs.wall2, inputs.wall3, inputs.wall4);
    try {
      await post(body);
      setShouldNavigate(true)
    } catch (error) {
      console.error('Erro ao processar a requisição:', error);
    }
  };

  const createComponent = () => {
    const elements = [];
    for (let i = 1; i <= 4; i++) {
      const wallKey = `wall${i}`;
      elements.push(
        <div key={wallKey} className={styles.card}>
          <h2>Parede {i}</h2>
          <CardMeasures id={wallKey} initialData={inputs[wallKey]} updateValues={updateValues} />
        </div>
      );
    }
    return elements;
  };

  return (
    <div className={styles.container}>
      <h1>Edite os valores de cada parede conforme a sua necessidade</h1>
      <form onSubmit={handleSubmit} className={styles.formContainer}>
        {createComponent()}
        {loading ? <Button textButton={"Carregando"} checkButton={true}/> : <Button textButton={"Calcular"} checkButton={false}/>}
        
      </form>
      {errorMessage && <Error message={errorMessage} />}
    </div>
  );
}

export default CalculatePaint;
