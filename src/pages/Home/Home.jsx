//CSS
import styles from "./Home.module.css"

//validator
import { validatorsMeasures } from "../../validators/validatorsMeasures"

//context
import { DataContext } from '../../context/DataContext';

//navigate
import { useNavigate } from 'react-router-dom';

//components
import Error from "../../components/Error/Error"
import Button from "../../components/Button/Button";

//hooks
import { useState, useContext } from "react"

const Home = () => {

  const [widthWall, setWidthWall] = useState("")
  const [heightWall, setHeightWall] = useState("")
  const [quantityDoor, setQuantityDoor] = useState("")
  const [quantityWindow, setQuantityWindow] = useState("")
  const [errorMessage, setErrorMessage] = useState("")

  const navigate = useNavigate();
  const { setData } = useContext(DataContext);

  const handleSubmit = (e) => {
    e.preventDefault()
    setErrorMessage("")

    const error = validatorsMeasures(widthWall, heightWall);
    if (error) {
      setErrorMessage(error);
    } else {
      setData({widthWall, heightWall, quantityDoor, quantityWindow});
      navigate('/calculador');
    }
  }

  return (
    <div className={styles.container}>
      <h1>Saiba a quantidade de tinta necessária para pintar a sua sala!</h1>
      <p>Digite as informações da sua primeira parede</p>
      <form onSubmit={handleSubmit} className={styles.formContainer}>

        <label for="largura">Informe a largura da primeira parede</label>
        <input type="number" required id="largura" placeholder="Largura" onChange={(e) => setWidthWall(e.target.value)} value={widthWall}/>

        <label for="altura">Informe a altura da primeira parede</label>
        <input type="number" required id="altura" placeholder="Altura" onChange={(e) => setHeightWall(e.target.value)} value={heightWall}/>

        <label for="porta">Informe a quantidade de portas da primeira parede</label>
        <input type="number" required id="porta" placeholder="Quantidade de portas" min="0" step="1" onChange={(e) => setQuantityDoor(e.target.value)} value={quantityDoor}/>

        <label for="janela">Informe a quantidade de janelas da primeira parede</label>
        <input type="number" required id="janela" placeholder="Quantidade de paredes" min="0" step="1" onChange={(e) => setQuantityWindow(e.target.value)} value={quantityWindow}/>

        <Button textButton={"Confirmar dados"}/>
      </form>
      {errorMessage && <Error message={errorMessage} />}
    </div>
  )
}

export default Home
