const apiUrl = "https://paint-calculator-digital-republic.onrender.com";

export const postData = async (endpoint, data) =>{
  try {
    const res = await fetch(`${apiUrl}/${endpoint}`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(data)
    })
    // if(res.statusCode === 500){
    //   throw new Error("Networking response was not ok")
    // }
    return await res.json()
  } catch (error) {
    console.log(error)
    throw new Error(`Houve um problema, ${error}`)
  }
}