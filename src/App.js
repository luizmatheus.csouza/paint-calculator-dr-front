// src/App.js
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { DataProvider } from './context/DataContext';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Home from './pages/Home/Home';
import CalculatePaint from './pages/CalculatePaint/CalculatePaint';
import Result from './pages/Result/Result';
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';

function App() {
  return (
    <DataProvider>
      <Router>
        <div className="App">
          <Header />
          <div className="containerGeral">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/calculador" element={<ProtectedRoute element={<CalculatePaint />} />} />
              <Route path="/resultado" element={<ProtectedRoute element={<Result />} />} />
            </Routes>
          </div>
          <Footer />
        </div>
      </Router>
    </DataProvider>
  );
}

export default App;
