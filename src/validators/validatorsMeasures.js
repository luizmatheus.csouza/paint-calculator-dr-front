export const validatorsMeasures = (widthWall, heightWall) =>{

  if(widthWall * heightWall < 1 || widthWall * heightWall >50){
    return `A parede não pode ser menor que 1m² e nem maior que 50m² no momento sua parede tem ${widthWall * heightWall}m²`
  }
  return null

}