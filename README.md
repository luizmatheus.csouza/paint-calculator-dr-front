# Meu Projeto

Este é o README para o meu projeto. Acesse agora o site através do link: <https://paint-calculator-phi.vercel.app>

## Descrição

Calculador de tinta do Code Challenge Digital Republic

## Instalação

Siga estas instruções para instalar e configurar o projeto em sua máquina local:

1. Clone o repositório do GitLab:

```bash
git clone https://gitlab.com/luizmatheus.csouza/paint-calculator-dr-front.git
```

2. Navegue até o diretório do projeto:

```bash
cd path/paint-calculator-dr-front
```

3. Instale as dependências do projeto usando npm ou yarn:

```bash
npm install
# ou
yarn install
```

## Uso

Siga estas instruções para rodar o projeto:

1. Execute o projeto:

```bash
npm start
# ou
yarn start
```